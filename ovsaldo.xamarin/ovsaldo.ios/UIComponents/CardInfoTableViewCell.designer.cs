﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace ovsaldo.ios.UIComponents
{
    [Register ("CardInfoTableViewCell")]
    partial class CardInfoTableViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CardIdLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CardNameLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (CardIdLabel != null) {
                CardIdLabel.Dispose ();
                CardIdLabel = null;
            }

            if (CardNameLabel != null) {
                CardNameLabel.Dispose ();
                CardNameLabel = null;
            }
        }
    }
}