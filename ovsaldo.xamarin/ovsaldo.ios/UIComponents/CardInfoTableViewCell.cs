﻿using System;

using Foundation;
using UIKit;

namespace ovsaldo.ios.UIComponents
{
    public partial class CardInfoTableViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("CardInfoTableViewCell");
        public static readonly UINib Nib;

        static CardInfoTableViewCell()
        {
            Nib = UINib.FromName("CardInfoTableViewCell", NSBundle.MainBundle);
        }

        protected CardInfoTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
    }
}
