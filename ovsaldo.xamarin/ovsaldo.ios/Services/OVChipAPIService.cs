using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OvChipApi;

namespace OVSaldo.ios.Services
{
    public class OVChipAPIService
    {
        private String Username;
        private String Password;

        public OVChipAPIService(String username, String password)
        {
            this.Username = username;
            this.Password = password;
        }

        public async Task GetCardsAsync()
        {
            using (var client = new OvClient())
            {
                bool loginResponse = await client.LoginAsync(Username, Password);

                Console.WriteLine(client.AuthorizationToken);
                Console.WriteLine(JsonConvert.SerializeObject(client.OAuthTokens, Formatting.Indented));
            }
        }
    }
}
